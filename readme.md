# The Git Limeric Game

- This repository is a game based on collaborative writing, and Git. 
- It is implemented using limericks, so no knowledge of any programming language is necessary.
- Learning and using Git *is* a technical task, but it is still accessible to non-developers.  Everybody can play.

### File List:
- `readme.md` - Yer lookin at it.  Change this file if you want to update the game documentation.  More importantly, change this file if you want to change the rules of the game.
- `lymryx.txt` - The source code of the project; a.k.a. the heart of our collective poetic genius.  It's a list of limericks.

### Source "code":
- `lymryx.txt` is the source code of our precious application, written to a specification called `LYMRYX`.
- A valid `LYMRYX` file consists of complete limericks (5 lines each), and/or in-progress limericks (up to five lines). 
- Complete limerics are separated by blank newlines.
- Comments (non-poetry lines) are allowed using any programming comment syntax known to man, including `//`,`#`,`/* */`,`--`,`<!-- -->` etc.  Comments can contain any content.

### LYMRYX developer notes
- Once a limerick is complete (5 lines), it is considered tested and ready for production.  This means we can merge it into `master`.
- Limericks with a broken rhyme scheme (AABBA) will compile, but can't really be considered tested or stable.
- Preferred formatting convention is to indent lines 3 and 4 by two spaces.

# Gameplay

- The goal of the game is to be the most prolific collaborative limerick writer of your time.  
- The trophy will rotate on a weekly basis ("your time" may be relatively short!).
- If you have the most points from Sunday to Sunday, you get the travelling trophy for the following week.

## Rules
- Development of a new limerick starts on a feature branch. Every player should maintain an active feature branch for their in-progress limericks.  
	- `<GitLabUsername>_feature` is the suggested convention.  My feature branch will be `mikepat_feature`.
- Commit only one new line (of poetry) at a time (per branch).  Whitespace and comments are excluded from this quota.
- You can commit to your own feature branch or anyone else's feature branch, at any time.  See the rule about alternating commits for the catch.
- You must add a new line (of limerick poetry) every time you commit.
- You can also (optionally) edit one existing line of any limerick "while you're in there".  
	- Just don't break the rhyme scheme.  
	- This allows for gradual refactoring of existing limericks.
	- Any commit can do anything with any number of comment lines.
- Commits must alternate between players on any given feature branch for a particualar feature (limerick).
	- You can't commit to any branch twice in a row, in the same day.  Somebody else has to push a change to that branch before you can again. This includes the feature branch you own.  However, if you commit on Monday, and come Tuesday nobody else has committed, you may commit again on Tuesday.  In other words, you can commit to every branch, every day, regardless of the actions (or inactions) of other players.
	- This implies that nobody writes two consecutive lines of any limerick; that's the "collaborative" part.
	- The last line of one limerick and the first line of the next do not count as consecutive, since the next is a new "feature".
- Once your active feature branch is five lines (and rhymes), you get it merged into master, and then you start again.
- No branch gets merged until the limerick is complete (that's when it's production-ready!).
- All branches get merged once a limerick is complete (we're not sandbagging features, are we?).
- Deleting lines is against the rules.  This does not apply to comments.  Existing lines of poetry can only be re-written; not deleted.

## Revising Rules
- Rules changes are proposed and evaluated via pull request.  
- If you'd like to change the rules, simply edit the `readme.md` file to your liking, and submit a PR.

## Scoring
- Is super simple: one point per commit.

- That's it (for now).  
  - No extra points (or deductions) for exotic branching strategies or other git antics.  
  - No extra points for clever wordplay or beautiful poetry (i.e. code quality).
  - No extra points for each time you get your feature branch merged into master.
  - No extra points for limericks with five authors (ultra-collab).
  - No extra points for introducing a new comment syntax.
		
- To maximize your score, just commit as often as you can to as many active branches as you can.

# Getting Started
- Clone this project.
- Create a local branch.
- Add the first line of a new limerick to the lymryx.txt file on your branch.
- Commit the change, and push your branch to origin.
- You are now at the mercy of other writers - someone else much check out your branch and add the next line to your limerick.
- Once another writer has contributed a line, you may contribute another line, or wait for another contributor.
- Once your limerick is complete, merge your branch with master and push.

# Tips for playing
- Try to commit every day.
- Look for any branches with in-progress limericks.  Push to all of them as soon as you can.
- Keep your own feature branch "in-flight" at all times.  You should always have a limerick in progress.
- Propose rule changes that will benefit you more than others.  Rule changes are proposed by editing the rules themselves in the readme and submitting a pull request.
- While the rhyme scheme is (kind of artificially) important, the quality of the poetry absolutely isn't.  You want to be the **most prolific**, not the *best*.
  - The point is to get used to Git and the tooling around it, so commit with reckless abandon! (within the aforementioned rules) 
  - Experiment with tooling - complete new limericks using VSCode, Eclipse, SourceTree, GitKraken, GitBash CLI, etc.
